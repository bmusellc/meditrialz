//
//  ComicsViewController.h
//  ORKSample
//
//  Created by Stanislav Kovalchuk on 6/17/16.
//  Copyright © 2016 Apple, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComicsNavigationController : UINavigationController

@end
