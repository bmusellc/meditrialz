//
//  PDFDocumentInfo.m
//  PDFPreview
//
//  Created by Stanislav Kovalchuk on 6/15/16.
//  Copyright © 2016 Stanislav Kovalchuk. All rights reserved.
//

#import "PDFDocumentInfo.h"
@import UIKit;

@interface PDFDocumentInfo ()

@property (nonatomic, strong) NSURL*              url;
@property (nonatomic, assign) NSInteger           numberOfPages;
@property (nonatomic, assign) CGPDFDocumentRef    pdfDocument;
@property (nonatomic, strong) NSArray*            frames;
@property (nonatomic, assign) CGSize              pageSize;

@end

@implementation PDFDocumentInfo

- (instancetype)initWithURL:(NSURL *)url
{
    self = [super init];
    if (self)
    {
        self.url = url;
        self.pdfDocument = CGPDFDocumentCreateWithURL((CFURLRef)self.url);
        self.numberOfPages = CGPDFDocumentGetNumberOfPages(self.pdfDocument);
        
        NSString* name = [[[url lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"json"];
        NSURL* jsonURL = [[NSBundle mainBundle]URLForResource:name withExtension:@""];
        NSDictionary *parsedData = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:jsonURL] options:kNilOptions error:nil];
        
        self.pageSize = CGSizeMake([parsedData[@"page_width"] floatValue], [parsedData[@"page_height"] floatValue]);
        NSArray<NSArray *>* pagesData = parsedData[@"pages"];
        NSMutableArray* frames = [NSMutableArray array];
        
        for (NSArray<NSDictionary *>* pageDataArray in pagesData)
        {
            NSMutableArray* page = [NSMutableArray array];
            for (NSDictionary* dic in pageDataArray)
            {
                CGRect pageRect = CGRectMake([dic[@"x"] floatValue],
                                             [dic[@"y"] floatValue],
                                             [dic[@"width"] floatValue],
                                             [dic[@"height"] floatValue]);
                [page addObject:[NSValue valueWithCGRect:pageRect]];
            }
            [frames addObject:[page copy]];
        }
                
        self.frames = [frames copy];
        
        
        
    }
    return self;
}

- (void)dealloc
{
    CGPDFDocumentRelease(self.pdfDocument);
    self.pdfDocument = nil;
}

@end
