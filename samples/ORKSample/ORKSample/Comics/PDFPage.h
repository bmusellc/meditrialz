//
//  PDFPage.h
//  PDFPreview
//
//  Created by Stanislav Kovalchuk on 6/14/16.
//  Copyright © 2016 Stanislav Kovalchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreGraphics;
@interface PDFPage : UIView

@property (nonatomic, assign) CGPDFDocumentRef pdf;
@property (nonatomic, assign) NSInteger pageNumber;
@property (nonatomic, readonly) CGSize pageSize;

- (void)reload;
- (void)highlightFrame:(CGRect)frame animated:(BOOL)animated;
- (void)hideHighlightsAnimated:(BOOL)animated;
@end
