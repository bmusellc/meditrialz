
//  PDFPage.m
//  PDFPreview
//
//  Created by Stanislav Kovalchuk on 6/14/16.
//  Copyright © 2016 Stanislav Kovalchuk. All rights reserved.
//

#import "PDFPage.h"

#define kOverlayColor [UIColor whiteColor]

@interface PDFTileLayer : CATiledLayer

@end

@implementation PDFTileLayer

+ (CFTimeInterval)fadeDuration
{
    return 0.1;
}

@end


@interface PDFPage ()

@property(nonnull, strong) NSLayoutConstraint* topOverlayHeight;
@property(nonnull, strong) NSLayoutConstraint* bottomOverlayHeight;
@property(nonnull, strong) NSLayoutConstraint* leftOverlaywidth;
@property(nonnull, strong) NSLayoutConstraint* rightOverlaywidth;

@property(nonatomic, assign) CGSize pageSize;

@end

@implementation PDFPage

+ (Class)layerClass
{
    return [PDFTileLayer class];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
//        [self _addOverlays];
        
        [(CATiledLayer *)self.layer setTileSize:CGSizeMake(256, 256)];
    }
    return self;
}

- (void)reload
{
    [self setNeedsLayout];
    [self setNeedsDisplay];
}

- (void)highlightFrame:(CGRect)frame animated:(BOOL)animated
{
    self.topOverlayHeight.constant = CGRectGetMinY(frame);
    self.bottomOverlayHeight.constant = CGRectGetHeight(self.bounds) - CGRectGetMaxY(frame);
    self.leftOverlaywidth.constant = CGRectGetMinX(frame);
    self.rightOverlaywidth.constant = CGRectGetWidth(self.bounds) - CGRectGetMaxX(frame);

    [self setNeedsLayout];
    if (animated)
    {
        [UIView animateWithDuration:0.3
                         animations:^
         {
             [self layoutIfNeeded];
         }];
    }
}

- (void)hideHighlightsAnimated:(BOOL)animated
{
    self.topOverlayHeight.constant = 0;
    self.bottomOverlayHeight.constant = 0;
    self.leftOverlaywidth.constant = 0;
    self.rightOverlaywidth.constant = 0;
    [self setNeedsLayout];
    if (animated)
    {
        [UIView animateWithDuration:0.3
                         animations:^
        {
            [self layoutIfNeeded];
        }];
    }
}

- (CGSize)pageSize
{
    if (CGSizeEqualToSize(_pageSize, CGSizeZero))
    {
        CGPDFPageRef page = CGPDFDocumentGetPage(self.pdf, self.pageNumber);
        CGRect mediaRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
        _pageSize = mediaRect.size;
    }
    
    return _pageSize;

}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
    CGPDFPageRef page = CGPDFDocumentGetPage(self.pdf, self.pageNumber);
    
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // PDF might be transparent, assume white paper
//    [[UIColor whiteColor] set];
//    CGContextFillRect(ctx, rect);
    
    // Flip coordinates
    CGContextGetCTM(ctx);
    CGContextScaleCTM(ctx, 1, -1);
    
    CGRect rect = self.bounds;
    
    CGContextTranslateCTM(ctx, 0, -rect.size.height);
    
    // get the rectangle of the cropped inside
    CGRect mediaRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
    
    self.pageSize = mediaRect.size;
    
    CGContextScaleCTM(ctx, rect.size.width / mediaRect.size.width,
                      rect.size.height / mediaRect.size.height);
    CGContextTranslateCTM(ctx, -mediaRect.origin.x, -mediaRect.origin.y);
    
    // draw it
    CGContextSetInterpolationQuality(ctx, kCGInterpolationHigh);
    CGContextSetRenderingIntent(ctx, kCGRenderingIntentDefault);
    CGContextDrawPDFPage(ctx, page);
}

//- (void)drawRect:(CGRect)rect
//{
//    CGPDFPageRef page = CGPDFDocumentGetPage(self.pdf, self.pageNumber);
//    
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
//    
//    // PDF might be transparent, assume white paper
//    [[UIColor whiteColor] set];
//    CGContextFillRect(ctx, rect);
//    
//    // Flip coordinates
//    CGContextGetCTM(ctx);
//    CGContextScaleCTM(ctx, 1, -1);
//    CGContextTranslateCTM(ctx, 0, -rect.size.height);
//    
//    // get the rectangle of the cropped inside
//    CGRect mediaRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
//    
//    self.pageSize = mediaRect.size;
//    
//    CGContextScaleCTM(ctx, rect.size.width / mediaRect.size.width,
//                      rect.size.height / mediaRect.size.height);
//    CGContextTranslateCTM(ctx, -mediaRect.origin.x, -mediaRect.origin.y);
//    
//    // draw it
//    CGContextDrawPDFPage(ctx, page);
//}

#pragma mark - Private
- (void)_addOverlays
{
    {
        UIView* topOverlay = [UIView new];
        topOverlay.backgroundColor = kOverlayColor;
        [self addSubview:topOverlay];
        topOverlay.translatesAutoresizingMaskIntoConstraints = NO;
        self.topOverlayHeight = [NSLayoutConstraint
                                       constraintWithItem:topOverlay
                                       attribute:NSLayoutAttributeHeight
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:nil
                                       attribute:NSLayoutAttributeNotAnAttribute
                                       multiplier:1.0
                                       constant:0.0];
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:topOverlay
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:0];
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:topOverlay
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        NSLayoutConstraint *trailing = [NSLayoutConstraint
                                        constraintWithItem:topOverlay
                                        attribute:NSLayoutAttributeTrailing
                                        relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                        attribute:NSLayoutAttributeTrailing
                                        multiplier:1.0f
                                        constant:0.f];
        
        [topOverlay addConstraint:self.topOverlayHeight];
        [self addConstraint:trailing];
        [self addConstraint:top];
        [self addConstraint:leading];
    }
    {
        UIView* bottomOverlay = [UIView new];
        bottomOverlay.backgroundColor = kOverlayColor;
        [self addSubview:bottomOverlay];
        bottomOverlay.translatesAutoresizingMaskIntoConstraints = NO;
        self.bottomOverlayHeight = [NSLayoutConstraint
                                 constraintWithItem:bottomOverlay
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                 multiplier:1.0
                                 constant:0.0];
        NSLayoutConstraint *bottom = [NSLayoutConstraint
                                   constraintWithItem:bottomOverlay
                                   attribute:NSLayoutAttributeBottom
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self
                                   attribute:NSLayoutAttributeBottom
                                   multiplier:1.0 constant:0];
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:bottomOverlay
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        NSLayoutConstraint *trailing = [NSLayoutConstraint
                                        constraintWithItem:bottomOverlay
                                        attribute:NSLayoutAttributeTrailing
                                        relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                        attribute:NSLayoutAttributeTrailing
                                        multiplier:1.0f
                                        constant:0.f];
        
        [bottomOverlay addConstraint:self.bottomOverlayHeight];
        [self addConstraint:trailing];
        [self addConstraint:bottom];
        [self addConstraint:leading];
    }
    {
        UIView* leftOverlay = [UIView new];
        leftOverlay.backgroundColor = kOverlayColor;
        [self addSubview:leftOverlay];
        leftOverlay.translatesAutoresizingMaskIntoConstraints = NO;
        self.leftOverlaywidth = [NSLayoutConstraint
                                    constraintWithItem:leftOverlay
                                    attribute:NSLayoutAttributeWidth
                                    relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                    attribute:NSLayoutAttributeNotAnAttribute
                                    multiplier:1.0
                                    constant:0.0];
        NSLayoutConstraint *bottom = [NSLayoutConstraint
                                      constraintWithItem:leftOverlay
                                      attribute:NSLayoutAttributeBottom
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:self
                                      attribute:NSLayoutAttributeBottom
                                      multiplier:1.0 constant:0];
        NSLayoutConstraint *top = [NSLayoutConstraint
                                      constraintWithItem:leftOverlay
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:self
                                      attribute:NSLayoutAttributeTop
                                      multiplier:1.0 constant:0];
        
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:leftOverlay
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        
        [leftOverlay addConstraint:self.leftOverlaywidth];
        [self addConstraint:top];
        [self addConstraint:bottom];
        [self addConstraint:leading];
    }
    {
        UIView* rightOverlay = [UIView new];
        rightOverlay.backgroundColor = kOverlayColor;
        [self addSubview:rightOverlay];
        rightOverlay.translatesAutoresizingMaskIntoConstraints = NO;
        self.rightOverlaywidth = [NSLayoutConstraint
                                 constraintWithItem:rightOverlay
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                 multiplier:1.0
                                 constant:0.0];
        NSLayoutConstraint *bottom = [NSLayoutConstraint
                                      constraintWithItem:rightOverlay
                                      attribute:NSLayoutAttributeBottom
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:self
                                      attribute:NSLayoutAttributeBottom
                                      multiplier:1.0 constant:0];
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:rightOverlay
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:0];
        
        NSLayoutConstraint *trailing = [NSLayoutConstraint
                                       constraintWithItem:rightOverlay
                                       attribute:NSLayoutAttributeTrailing
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self
                                       attribute:NSLayoutAttributeTrailing
                                       multiplier:1.0f
                                       constant:0.f];
        
        [rightOverlay addConstraint:self.rightOverlaywidth];
        [self addConstraint:top];
        [self addConstraint:bottom];
        [self addConstraint:trailing];
    }

}

@end
