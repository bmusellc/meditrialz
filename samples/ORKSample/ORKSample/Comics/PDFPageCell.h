//
//  PDFPageCell.h
//  PDFPreview
//
//  Created by Stanislav Kovalchuk on 6/15/16.
//  Copyright © 2016 Stanislav Kovalchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PDFPage;
@class PDFPageCell;

@protocol PDFPageCellDelegate

- (void)pdfCell:(PDFPageCell *)sender asksToHighlightFrame:(CGRect)frame;

@end

@interface PDFPageCell : UICollectionViewCell

@property (nonatomic, assign) NSObject<PDFPageCellDelegate>*    delegate;
@property (strong, nonatomic) IBOutlet PDFPage *pdfPage;
@property (strong, nonatomic) NSArray          *frames;

- (BOOL)highlightNextFrame;
- (BOOL)highlightPreviousFrame;
- (void)highlightFrameWithIndex:(NSUInteger)index;
- (void)highlightFrame:(CGRect)frame animated:(BOOL)animated;
- (void)hideHighlightsAnimated:(BOOL)animated;

+ (NSString *)reuseID;

@end
