//
//  PDFPageCell.m
//  PDFPreview
//
//  Created by Stanislav Kovalchuk on 6/15/16.
//  Copyright © 2016 Stanislav Kovalchuk. All rights reserved.
//

#import "PDFPageCell.h"
#import "PDFPage.h"

@interface PDFPageCell ()

@property (strong, nonatomic) UITapGestureRecognizer*   doubleTapRecognizer;
@property (assign, nonatomic) NSInteger                 currentrlyHighlightedFrameIndex;
@end

@implementation PDFPageCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.currentrlyHighlightedFrameIndex = -1;
    }
    return self;
}


+ (NSString *)reuseID
{
    return NSStringFromClass([PDFPageCell class]);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.doubleTapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(_handleDoubleTap)];
    self.doubleTapRecognizer.numberOfTapsRequired = 2;
    [self.pdfPage addGestureRecognizer:self.doubleTapRecognizer];
}

- (BOOL)highlightPreviousFrame
{
    
    self.currentrlyHighlightedFrameIndex = MAX(-1, self.currentrlyHighlightedFrameIndex - 1);
    [self highlightFrameWithIndex:self.currentrlyHighlightedFrameIndex];
    
    return !(self.currentrlyHighlightedFrameIndex < (NSInteger)0);
}

- (BOOL)highlightNextFrame
{
    if (self.currentrlyHighlightedFrameIndex >= (NSInteger)[self.frames count])
    {
        return NO;
    }

    self.currentrlyHighlightedFrameIndex++;
    [self highlightFrameWithIndex:self.currentrlyHighlightedFrameIndex];
    
    return !(self.currentrlyHighlightedFrameIndex >= (NSInteger)[self.frames count]);
}
- (void)highlightFrameWithIndex:(NSUInteger)index
{
    if (index >= [self.frames count])
    {
        return;
    }
    
    CGRect rect = [self.frames[index] CGRectValue];
    CGAffineTransform transform = CGAffineTransformMakeScale(self.pdfPage.bounds.size.width / self.pdfPage.pageSize.width, self.pdfPage.bounds.size.height / self.pdfPage.pageSize.height);
    rect = CGRectApplyAffineTransform(rect, transform);
    [self _highlightFrame:rect];
}

- (void)highlightFrame:(CGRect)frame animated:(BOOL)animated
{
    [self.pdfPage highlightFrame:frame animated:animated];
}

- (void)hideHighlightsAnimated:(BOOL)animated
{
    [self.pdfPage hideHighlightsAnimated:animated];
}

#pragma mark - Private

- (void)_highlightFrame:(CGRect)frame
{
    if (!CGRectEqualToRect(frame, CGRectZero))
    {
//        [self.pdfPage highlightFrame:frame animated:YES];
        if (self.delegate && [self.delegate respondsToSelector:@selector(pdfCell:asksToHighlightFrame:)])
        {
            [self.delegate pdfCell:self asksToHighlightFrame:frame];
        }
    }
}

- (void)_handleDoubleTap
{
    CGPoint tapLocation = [self.doubleTapRecognizer locationInView:self.pdfPage];
    CGRect zoomRect = [self _hitRectForLocation:tapLocation];
    
    [self _highlightFrame:zoomRect];
}

- (CGRect)_hitRectForLocation:(CGPoint)location
{
    for (NSValue* val in self.frames)
    {
        CGRect r = [val CGRectValue];
        CGAffineTransform transform = CGAffineTransformMakeScale(self.pdfPage.bounds.size.width / self.pdfPage.pageSize.width, self.pdfPage.bounds.size.height / self.pdfPage.pageSize.height);
        r = CGRectApplyAffineTransform(r, transform);
        if (CGRectContainsPoint(r, location))
        {
            return r;
        }
    }
    
    return CGRectZero;
}

- (void)setFrames:(NSArray *)frames
{
    _frames = frames;
    
#ifdef DEBUG
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
//    {
//        [self _createTestViews];
//    });
#endif
}

- (void)_createTestViews
{
    CGAffineTransform transform = CGAffineTransformMakeScale(self.pdfPage.bounds.size.width / self.pdfPage.pageSize.width, self.pdfPage.bounds.size.height / self.pdfPage.pageSize.height);
    
    for (NSValue* val in self.frames)
    {
        CGRect rect = [val CGRectValue];
        rect = CGRectApplyAffineTransform(rect, transform);
        
        UIView* view = [UIView new];
        view.frame = rect;
        view.layer.borderWidth = 1;
        view.layer.borderColor = [UIColor redColor].CGColor;
        view.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
        [self.pdfPage addSubview:view];
    }
}

@end
