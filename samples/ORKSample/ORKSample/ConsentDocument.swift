/*
Copyright (c) 2015, Apple Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3.  Neither the name of the copyright holder(s) nor the names of any contributors
may be used to endorse or promote products derived from this software without
specific prior written permission. No license is granted to the trademarks of
the copyright holders even if such marks are included in this software.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import ResearchKit

class ConsentDocument: ORKConsentDocument {
    // MARK: Properties
    
    let ipsum = [
        "The purpose of this study is to evaluate a new treatment in children 6 to 11 years of age with uncontrolled persistent asthma. Asthma is a chronic inflammatory disease of the airways characterized by airway hyper responsiveness, acute and chronic bronchoconstriction, airway edema, and mucus plugging. The inflammatory component of asthma involves many cell types, including mast cells, eosinophils, T-lymphocytes, neutrophils, and epithelial cells and their biological products. For most asthma patients, a regimen of controller therapy and reliever therapy provides adequate longterm control. The majority of children with asthma have mild or moderate disease and can obtain adequate asthma control through avoidance of triggering factors and/or with the help of medications, such as short-acting inhaled β2-receptor agonists, inhaled corticosteroids (ICSs) and, when needed, addition of long-acting β2-receptor agonists and leukotriene receptor antagonists. However, 2–5% of all asthmatic children have uncontrolled asthma despite maximum treatment with conventional medications. Children with such severe symptoms are heterogeneous with respect to trigger factors, pulmonary function, inflammatory pattern and clinical symptoms. These children have a reduced quality of life, account for a large proportion of the healthcare costs related to asthma and represent a continuous clinical challenge to the pediatrician. Additionally, the long term adverse effects of systemic and inhaled corticosteroids on bone metabolism, adrenal function, and growth in children lead to attempts to minimize the amount of corticosteroid usage. Lastly, the consequences of unresponsiveness to therapy or lack of compliance with therapy is loss of asthma control and ultimately, asthma exacerbation. Signaling initiated by both IL-4 and IL-13.",
        "This screen will provide more information about which data is collected and how it is used.",
        "This screen will provide more information about patient and parent privacy.",
        "This screen will provide more information about how data is used.",
        "This screen will provide more information about the time commitment required from patients and their parents.",
        "This screen will provide more information about the survey questions asked during the study.",
        "This screen will provide more information about the types of activities we will offer your child during the study.",
        "We will not collect or store any new data if you choose to withdraw. To withdraw from the study, simply select \"Leave Study\" on the Profile tab."
    ]
    let sums = [
        "Let's start with an overview of how this app works.",
        "Throughout this study the app will collect information from your iPhone to help us send you and your child relevant material, to remind you about future appointments and to let your child track their progress on various games and activities.",
        "We take your privacy and the privacy of your child very serisouly. We will replace all personal identifiable information with a random code. The coded data will be encrypted and stored on a secure cloud server under the control of Sanofi to prevent improper access.",
        "Your coded study data will be used for research by the Sanofi Group.",
        "This study takes place place over the course of 52 weeks and will require a visit to the doctor's office or to a lab once every two weeks.",
        "We will ask you to answer brief surveys about how you and your child are feeling during the study period.",
        "During the study we will provide your child with fun activities to keep them motivated and engaged in the study and help them understand it better.",
        "You may withdraw your consent and discontinue participation at any time.",
    ]
    
    // MARK: Initialization
    
    override init() {
        super.init()
        
        title = NSLocalizedString("Research Health Study Consent Form", comment: "")
        
        let sectionTypes: [ORKConsentSectionType] = [
            .Overview,
            .DataGathering,
            .Privacy,
            .DataUse,
            .TimeCommitment,
            .StudySurvey,
            .StudyTasks,
            .Withdrawing
        ]
        
        sections = Array<ORKConsentSection>();
        for index in 0...sectionTypes.count - 1 {
            let sectionType = sectionTypes[index]
            let ips = ipsum[index]
            let sum = sums[index]
            let section = ORKConsentSection(type: sectionType)
            
            let localizedIpsum = NSLocalizedString(ips, comment: "")
            let localizedSummary = NSLocalizedString(sum, comment: "")
            
            section.summary = localizedSummary
            section.content = localizedIpsum
            sections?.append(section)
        }
        
        let signature = ORKConsentSignature(forPersonWithTitle: nil, dateFormatString: nil, identifier: "ConsentDocumentParticipantSignature")
        addSignature(signature)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ORKConsentSectionType: CustomStringConvertible {

    public var description: String {
        switch self {
            case .Overview:
                return "Overview"
                
            case .DataGathering:
                return "DataGathering"
                
            case .Privacy:
                return "Privacy"
                
            case .DataUse:
                return "DataUse"
                
            case .TimeCommitment:
                return "TimeCommitment"
                
            case .StudySurvey:
                return "StudySurvey"
                
            case .StudyTasks:
                return "StudyTasks"
                
            case .Withdrawing:
                return "Withdrawing"
                
            case .Custom:
                return "Custom"
                
            case .OnlyInDocument:
                return "OnlyInDocument"
        }
    }
}